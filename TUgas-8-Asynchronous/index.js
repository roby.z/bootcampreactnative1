// di index.js
let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let inputWaktu = 10000;
readBooks(inputWaktu, books[0], function(time){
    readBooks(time, books[1], function(time){
        readBooks(time, books[2], function(time){
            readBooks(time, books[0], function(time){
                return(time)
            })
        })
    })
})