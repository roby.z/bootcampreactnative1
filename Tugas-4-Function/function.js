console.log("No. 1")// No. 1
/*
   No. 1 (Function teriak), Saya pakai function sederhana tanpa return
*/
function teriak() {
    console.log("Halo Sanbers!");
  }
   
  teriak(); 

console.log(" ")
console.log("No. 2")// No. 2
/*
  No.2 (Function kalikan), Saya pakai function sederhana dengan return dan pengiriman parameter lebih dari satu
*/
var num1 = 12;
var num2 = 4;

function kalikan(){
    return num1*num2
}
var hasilkali = kalikan(num1, num2)
console.log(hasilkali);

console.log(" ")
console.log("No. 3")// No. 3
/*
    No.3 (Function introduce), Saya pakai function sederhana dengan return dan pengiriman parameter lebih dari satu
*/
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

function introduce(){
    var string = ("Nama saya "+name+", umur saya "+age+" tahun, "+"alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!")
    return string
}
var perkenalkan= introduce(name, age, address, hobby)
console.log(perkenalkan);

