console.log("Soal No. 1 (Array to Object)")
// var now = new Date()
// var thisYear = now.getFullYear()

function arrayToObject(arr) {
    // Code
    if (arr.length<=0){
        return console.log("")
    }
    for (var i=0;i<arr.length;i++){
        thisObject = {}

        var birthYear =arr[i][3];
        var now = new Date().getFullYear()
        var thisAge;

        if (birthYear && now - birthYear>0){
            thisAge= now - birthYear
        } else{
            thisAge="Invalid Birth Year"
        }
        thisObject.firstName = arr[i][0]
        thisObject.lastName = arr[i][1]
        thisObject.gender = arr[i][2]
        thisObject.age = thisAge

        var consolethisObject = (i +1)+" "+thisObject.firstName+" "+thisObject.lastName+" :"
        console.log(consolethisObject)
        console.log(thisObject)
    }
  }
  
  // Driver Code
  var people=([["Bruce", "Banner", "Male", 1975], ["Natasha", "Romanoff", "female"]]); // 1. Christ Evans:
  arrayToObject(people)
  /*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
console.log(" ");
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 

  arrayToObject([]);

console.log(" ");
console.log("Soal No. 2 (Shopping Time)")
function shoppingTime(memberId, money){
    // code
    if(!memberId){
        return("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if(money<50000){
        return("Mohon maaf, uang tidak cukup")
    } else{
        var nowObject = {};
        var uangKembalian = money;
        var listBarang = [];

        var sepatuStacattu = "Sepatu Brand Stacattu";
        var bajuZoro = "Baju Brand Zoro";
        var bajuHdanN = "Baju Brand H&N";
        var sweaterUniklooh = "Sweater brand Uniklooh";
        var casingHandphone = "Casing Handphone";

        var check = 0;
        
        for (var i=0; uangKembalian>=50000 && check==0;i++){
            if(uangKembalian>=1500000){
                listBarang.push=(sepatuStacattu)
                uangKembalian-=1500000 
            } else if (uangKembalian>=500000){
                listBarang.push=(bajuZoro)
                uangKembalian-=500000
            } else if(uangKembalian>=250000){
                listBarang.push=(bajuHdanN)
                uangKembalian-=250000
            }else if(uangKembalian>=175000){
                listBarang.push=(sweaterUniklooh)
                uangKembalian-=175000
            }else if(uangKembalian>=50000){
                if (listBarang.length !=0){
                   for(var j=0;j<=listBarang.length-1;j++){
                       if(listbarang[j]==casingHandphone){
                           check+=1;
                       }
                   } 
                   if (check=0){
                       listBarang.push=(casingHandphone)
                       uangKembalian-=50000
                   }
                } else {
                    listBarang.push(casingHandphone)
                    uangKembalian-=50000
                }
            }
            nowObject.memberId = memberId
            nowObject.money = money
            nowObject.listPurchased = listBarang
            nowObject.changeMoney = uangKembalian
            
            return nowObject;
        }
    }
}
//Test Cases
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
  console.log(shoppingTime("82Ku8Ma742", 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


  console.log(" ");
  console.log("Soal No. 3 (Naik Angkot)")
  function naikAngkot(arrPenumpang){
      rute =["A", "B", "C", "D", "E", "F"];
      arrOfouput=[];
    // code
    if (arrPenumpang.length<=0){
        return[]
    }
    for (var i=0;i <arrPenumpang.length;i++){
        var objOfoutput ={}
        var asal = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]

        var indexAsal ;
        var indexTujuan ;

        for(var j=0;j<rute.length;j++){
            if(rute[j]==asal){
                indexAsal=j
            } else if(rute[j]==tujuan){
                indexTujuan=j
            }
        }
        var bayar=(indexTujuan-indexAsal)*2000

        objOfoutput.Penumpang=arrPenumpang[i][0]
        objOfoutput.naikDari=asal
        objOfoutput.tujuan=tujuan
        objOfoutput.bayar=bayar

        arrOfouput.push(objOfoutput)
    }
        return arrOfouput;
  }
  //TEST CASE
console.log(naikAngkot([["Dimitri", "B", "F"], ["Icha", "A", "B"]]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]