console.log("No.1 Mengubah fungsi menjadi fungsi arrow")
/*const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()
*/

const golden = ()=>{
  console.log("this is golden")
}
golden()

console.log(" ")
console.log("No. 2 Sederhanakan menjadi Object literal di ES6")
/*return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, ubahlah menjadi bentuk yang lebih sederhana di ES6.

const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName()
*/

const newFunction = (firstName, lastName) =>{
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: ()=>{
        console.log(firstName+" "+lastName)
      }
    }
}

newFunction("William", "Imoh").fullName()

console.log(" ")
console.log("No. 3 Destructing")
/*
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:

const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)

// Driver code
console.log(firstName, lastName, destination, occupation)
*/
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
}
  const {firstName, lastName, destination, occupation, spell} = newObject

  console.log(firstName, lastName, destination, occupation)

  console.log(" ")
  console.log("No. 4 Array Spreading")
/*
  Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined =[...west, ...east]

console.log(combined)

console.log(" ")
  console.log("No. 5 Template Literals")
  // const firstName1 = "Zell"
  // const lastName1 = "Liew"
  // const teamName1= "una"
  // const thestring1= `${firstName} ${lastName}, ${teamName1}` 
  
  // console.log(thestring1)

/* 
sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before)
*/

const planet = "earth"
const view = "glass"
const before =  `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code

console.log(before)

