console.log("Soal No. 1 (Range)") // No. 1
function range(startNum, finishNum){
    if(startNum == null||finishNum == null ){
        return -1;
    }else{
        var soal = []
        if(startNum<finishNum){
            for (var i=startNum; i<=finishNum; i++) {
                soal.push(i)
            }
        }else{
            for (var i=startNum; i>=finishNum; i--) {
                soal.push(i);
         }
        }
        return soal;
    }
}
console.log(range(1, 10))//[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range())// -1 

console.log(" ")
console.log("Soal No. 2 (Range with Step)") // No. 2
function rangeWithStep(startNum, finishNum, step) {
    var soal =[]
    if(startNum<finishNum){
        for (var i=startNum; i<=finishNum; i+=step) {
            soal.push(i);
        }
    }else{
        for (var i=startNum; i>=finishNum; i-=step) {
            soal.push(i);
        }
    }
    return soal;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log(" ")
console.log("Soal No. 3 (Sum of Range)") // No. 3
function sum(startNum, finishNum, step) {
	var soal = 0;
	if(startNum == null && finishNum == null){
		return 0;
	}
	else if(startNum==null){
		return finishNum;
	}
	else if(finishNum==null){
		return startNum;
	}
	else{
		if(step == null){
			step = 1;
		}
		if(startNum<finishNum){
		   for(var i = startNum; i <= finishNum; i+=step) {
			   soal += i;
		   }
	   }
	   else{
		   for(var i = startNum; i >= finishNum; i-=step) {
			   soal += i;
			   }
		   }
	   return soal;
	}
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0  

console.log(" ")
console.log("Soal No. 4 (Array Multidimensi)") // No. 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data){
    for (var i=0; i< data.length; i++){
        console.log("Nomor ID: " + data[i][0])
        console.log("Nama Lengkap: " + data[i][1])
        console.log("TTL: " + data[i][2] + " " + data[i][3])
        console.log("Hobi: " + data[i][4])
        console.log("")
    };
}

dataHandling(input)

console.log(" ")
console.log("Soal No. 5 (Balik Kata)") // No. 5
function balikKata(kata) {
        var hasil = ""
    for (var i= kata.length-1 ; i>= 0; i--) {
        hasil +=kata[i]
    }
    return hasil;
}
console.log("No. 5")
console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

console.log(" ")
console.log("Soal No. 6 (Metode Array)") // No. 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);
function dataHandling2(input) {
    input.splice(1,1,input[1]+"Elsharawy")
    input.splice(2,1,"Provinsi "+input[2])
    input.splice(4,1,"Pria", "SMA Internasional Metro")
    console.log(input)

    var ttl = String(input.slice(3,4));
    var bulan = ttl.split("/");
    switch (bulan[1]) {
        case "01":
            var bulanhuruf ="Januari"; break;
        case "02":
            var bulanhuruf = "Februari"; break;
        case "03":
            var bulanhuruf="Maret"; break;
        case "04":
            var bulanhuruf="April"; break;
        case "05":
            var bulanhuruf="Mei"; break;
        case "06":
            var bulanhuruf="Juni"; break;
        case "07":
            var bulanhuruf="Juli"; break;
        case "08":
            var bulanhuruf="Agustus"; break;
        case "09":
            var bulanhuruf="September"; break;
        case "10":
            var bulanhuruf="Oktober"; break;
        case "11":
            var bulanhuruf="November"; break;
        case "12":
            var bulanhuruf="Desember"; break;
        default:
            break;
    }
    console.log(bulanhuruf)
    console.log(bulan.sort(function(value1,value2){return value2-value1}));
    var bulan = ttl.split("/");
    console.log(bulan.join("-"));
    var nama = String(input.slice(1,2));
	console.log(nama.slice(0,14));

    
}