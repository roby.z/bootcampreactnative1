console.log("No. 1 Animal Class")
// Release 0
class Animal {
    constructor(name,legs = 4,cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
} 
var sheep = new Animal("shaun"); 
console.log(sheep.name) 
console.log(sheep.legs) 
console.log(sheep.cold_blooded) 

// Release 1
class Ape {
    constructor(name,kaki = 2){
        this.name = name
        this.kaki = kaki
    }
    yell(){
        var yell;
        if(this.kaki == 2){ yell = "Auooo" } 
        else { yell = "Ntah apa"}
        return console.log(yell)
    }
}
class Frog {
    constructor(name){
        this.name
    }
    jump(){
        return console.log("hop hop")
    }
} 
var sungokong = new Ape("kera sakti")
sungokong.yell() 
var kodok = new Frog("buduk")
kodok.jump()

console.log("")
console.log("No. 2 Function to Class")
// function Clock({ template }) {

//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
// }
  
// var clock = new Clock({template: 'h:m:s'});
// clock.start(); 

class Clock {
    constructor(template){
        this.template = template.template        
    }
    render() {
        var date = new Date();
        
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
        
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        if (this.template != undefined){
            var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        } else {
            var output = hours.toString() + ":" + mins + ":" + secs;
        }
        
        console.log(output);
    }
    stop(){
        let timer;
        clearInterval(timer);
    }
    start(){
        let timer;
        this.render();
        timer = setInterval(this.render, 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();

